package hibernatedemo.main;

import hibernatedemo.model.*;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.util.List;
import java.util.Set;

/**
 * Created by Chien Nguyen on 22/06/2017.
 */
public class App {
    private static SessionFactory sessionFactory;

    protected static void cofig(){
        Configuration config = new Configuration().configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();
        sessionFactory = config.buildSessionFactory(serviceRegistry);
    }

    public static void main(String[] args) {
        cofig();
        Session session = sessionFactory.openSession();
        session.beginTransaction();
//        insertToDB(session);
//        Customer customer = new Customer("Customer 9", "hihi", "hihi");
//        session.save(customer);
//        displayCustomer(session);
//        Customer customer = (Customer) session.get(Customer.class, 7);
//        Payment payment = (Payment) session.get(Payment.class, new PaymentPK(customer, "3"));
//        customer.setCity("Ha Noi");

//        session.delete(customer);
//        session.delete(payment);
//        insertToDB(session);
//        Customer customer = (Customer) session.get(Customer.class, 6);
//        session.delete(customer);

        Query query = session.createQuery("from " + Customer.class.getName() + " where customerNumber = 7");

//        Integer test = query.getFirstResult();
//        System.out.println(test);
//
        List<Customer> customers = query.list();
        for (Customer customer: customers) {
            System.out.println(customer.getContactFirstName());
        }
//        Customer customer = query.

        session.getTransaction().commit();
        session.close();
        System.out.println("Xong");
    }

    public static void insertToDB(Session session){
        // Khởi tạo office
        Office office0 = new Office("1","Ha Noi 0", "Viet Nam", "10000");
        Office office1 = new Office("2","Ha Noi 1", "Viet Nam", "10000");
        Office office2 = new Office("3","Ha Noi 2", "Viet Nam", "10000");
        Office office3 = new Office("4","Ha Noi 3", "Viet Nam", "10000");
        Office office4 = new Office("5","Ha Noi 4", "Viet Nam", "10000");

        session.save(office0);
        session.save(office1);
        session.save(office2);
        session.save(office3);
        session.save(office4);


        // Khơi tạo employee
        Employee emp0 = new Employee("Employee 0", "K");
        emp0.setOffice(office0);
        Employee emp1 = new Employee("Employee 1", "B");
        emp1.setOffice(office1);
        Employee emp2 = new Employee("Employee 2", "H");
        emp2.setOffice(office2);
        Employee emp3 = new Employee("Employee 3", "H");
        emp3.setOffice(office3);
        Employee emp4 = new Employee("Employee 4", "H");
        emp4.setOffice(office4);

        session.save(emp0);
        session.save(emp1);
        session.save(emp2);
        session.save(emp3);
        session.save(emp4);


        // Khoi tao product line
        ProductLine proL0 = new ProductLine("0");
        ProductLine proL1 = new ProductLine("1");
        ProductLine proL2 = new ProductLine("2");
        ProductLine proL3 = new ProductLine("3");
        ProductLine proL4 = new ProductLine("4");

        session.save(proL0);
        session.save(proL1);
        session.save(proL2);
        session.save(proL3);
        session.save(proL4);

        // Khoi tao product
        Product product0 = new Product("0","productt 0");
        product0.setProductLine(proL0);
        Product product1 = new Product("1","productt 1");
        product1.setProductLine(proL1);
        Product product2 = new Product("2","productt 2");
        product2.setProductLine(proL2);
        Product product3 = new Product("3","productt 3");
        product3.setProductLine(proL3);
        Product product4 = new Product("4","productt 4");
        product4.setProductLine(proL4);

        session.save(product0);
        session.save(product1);
        session.save(product2);
        session.save(product3);
        session.save(product4);


        // Khoi tao customer
        Customer customer0 = new Customer("Customer 0", "A", "B");
        customer0.setSalesRepEmployee(emp0);
        Customer customer1 = new Customer("Customer 1", "A", "B");
        customer1.setSalesRepEmployee(emp1);
        Customer customer2 = new Customer("Customer 2", "A", "B");
        customer2.setSalesRepEmployee(emp2);
        Customer customer3 = new Customer("Customer 3", "A", "B");
        customer3.setSalesRepEmployee(emp3);
        Customer customer4 = new Customer("Customer 4", "A", "B");
        customer4.setSalesRepEmployee(emp4);

        session.save(customer0);
        session.save(customer1);
        session.save(customer2);
        session.save(customer3);
        session.save(customer4);

        // khoi tao order
        Order order0 = new Order(customer0);
        Order order1 = new Order(customer1);
        Order order2 = new Order(customer2);
        Order order3 = new Order(customer3);
        Order order4 = new Order(customer4);

        session.save(order0);
        session.save(order1);
        session.save(order2);
        session.save(order3);
        session.save(order4);


        // Khoi tao payment
        Payment payment0 = new Payment(customer0, "1");
        Payment payment1 = new Payment(customer1, "2");
        Payment payment2 = new Payment(customer2, "3");
        Payment payment3 = new Payment(customer3, "4");
        Payment payment4 = new Payment(customer4, "5");

        session.save(payment0);
        session.save(payment1);
        session.save(payment2);
        session.save(payment3);
        session.save(payment4);

        // Khoi tao order detail
        OrderDetail orderDetail0 = new OrderDetail(order0, product0);
        OrderDetail orderDetail1 = new OrderDetail(order1, product1);
        OrderDetail orderDetail2 = new OrderDetail(order2, product2);
        OrderDetail orderDetail3 = new OrderDetail(order3, product3);
        OrderDetail orderDetail4 = new OrderDetail(order4, product4);

        session.save(orderDetail0);
        session.save(orderDetail1);
        session.save(orderDetail2);
        session.save(orderDetail3);
        session.save(orderDetail4);
    }

    public static void displayCustomer(Session session){
        List<Customer> customers = session.createCriteria(Customer.class).list();
        System.out.println("------Ket qua-------");
        for (Customer customer : customers){
            System.out.println(customer.getCustomerNumber() + " Name: " + customer.getCustomerName());
            System.out.println("order:");
            Set<Order> orders = customer.getOrders();
            for (Order order : orders){
                System.out.println("\t" + order.getOrderNumber());
                Set<OrderDetail> orderDetails = order.getOrderDetails();
                for (OrderDetail orderDetail : orderDetails){
                    System.out.println("\t\t" + orderDetail.getProduct().getProductName());
                }
            }
            System.out.println("\tsaleRepEmployee:");
            Employee emp = customer.getSalesRepEmployee();
            if(emp != null){
                System.out.println(emp.getFirstName() + " " + emp.getLastName() + " " + emp.getOffice().getOfficeCode());
            }
        }
    }
}
