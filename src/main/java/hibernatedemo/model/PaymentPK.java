package hibernatedemo.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Chien Nguyen on 23/06/2017.
 */
@Embeddable
public class PaymentPK implements Serializable {
    @ManyToOne
    @JoinColumn(name = "customerNumber")
    protected Customer customer;

    @Column(name = "checkNumber")
    protected String checkNumber;

    public PaymentPK() {
    }

    public PaymentPK(Customer customer, String checkNumber) {
        this.customer = customer;
        this.checkNumber = checkNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PaymentPK)) return false;

        PaymentPK paymentPK = (PaymentPK) o;

        if (customer != null ? !customer.equals(paymentPK.customer) : paymentPK.customer != null) return false;
        return checkNumber != null ? checkNumber.equals(paymentPK.checkNumber) : paymentPK.checkNumber == null;
    }

    @Override
    public int hashCode() {
        int result = customer != null ? customer.hashCode() : 0;
        result = 31 * result + (checkNumber != null ? checkNumber.hashCode() : 0);
        return result;
    }
}
