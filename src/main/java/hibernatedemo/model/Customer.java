package hibernatedemo.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by Chien Nguyen on 21/06/2017.
 */
@Entity
@Table(name = "customers")
public class Customer implements Serializable{
    @Id
    @GeneratedValue
    @Column(name = "customerNumber")
    private Integer customerNumber;

    @Column(name = "customerName", length = 50, nullable = false)
    private String customerName;

    @Column(name = "contactLastName")
    private String contactLastName;

    @Column(name = "contactFirstName")
    private String contactFirstName;

    @Column(name = "phone")
    private String phone;

    @Column(name = "addressLine1")
    private String addressLine1;

    @Column(name = "addressLine2")
    private String addressLine2;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "postalCode")
    private String postalCode;

    @Column(name = "country")
    private String country;

    @ManyToOne
    @JoinColumn(name = "salesRepEmployeeNumber")
    private Employee salesRepEmployee;

    @Column(name = "creditLimit")
    private Double creditLimit;

    @OneToMany(mappedBy = "customer", orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<Order> orders;

    @OneToMany(mappedBy = "paymentPK.customer", orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<Payment> payments;

    public Customer() {
    }

    public Customer(String customerName, String contactLastName, String contactFirstName) {
        this.customerName = customerName;
        this.contactLastName = contactLastName;
        this.contactFirstName = contactFirstName;
    }

    public Integer getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(Integer customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getContactLastName() {
        return contactLastName;
    }

    public void setContactLastName(String contactLastName) {
        this.contactLastName = contactLastName;
    }

    public String getContactFirstName() {
        return contactFirstName;
    }

    public void setContactFirstName(String contactFirstName) {
        this.contactFirstName = contactFirstName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Employee getSalesRepEmployee() {
        return salesRepEmployee;
    }

    public void setSalesRepEmployee(Employee salesRepEmployee) {
        this.salesRepEmployee = salesRepEmployee;
    }

    public Double getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(Double creditLimit) {
        this.creditLimit = creditLimit;
    }

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }

    public Set<Payment> getPayments() {
        return payments;
    }

    public void setPayments(Set<Payment> payments) {
        this.payments = payments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Customer)) return false;

        Customer customer = (Customer) o;

        if (getCustomerNumber() != null ? !getCustomerNumber().equals(customer.getCustomerNumber()) : customer.getCustomerNumber() != null)
            return false;
        if (getCustomerName() != null ? !getCustomerName().equals(customer.getCustomerName()) : customer.getCustomerName() != null)
            return false;
        if (getContactLastName() != null ? !getContactLastName().equals(customer.getContactLastName()) : customer.getContactLastName() != null)
            return false;
        if (getContactFirstName() != null ? !getContactFirstName().equals(customer.getContactFirstName()) : customer.getContactFirstName() != null)
            return false;
        if (getCreditLimit() != null ? !getCreditLimit().equals(customer.getCreditLimit()) : customer.getCreditLimit() != null)
            return false;
        if (getOrders() != null ? !getOrders().equals(customer.getOrders()) : customer.getOrders() != null)
            return false;
        return getPayments() != null ? getPayments().equals(customer.getPayments()) : customer.getPayments() == null;
    }

    @Override
    public int hashCode() {
        int result = getCustomerNumber() != null ? getCustomerNumber().hashCode() : 0;
        result = 31 * result + (getCustomerName() != null ? getCustomerName().hashCode() : 0);
        result = 31 * result + (getContactLastName() != null ? getContactLastName().hashCode() : 0);
        result = 31 * result + (getContactFirstName() != null ? getContactFirstName().hashCode() : 0);
        return result;
    }
}
