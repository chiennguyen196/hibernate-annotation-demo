package hibernatedemo.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Chien Nguyen on 21/06/2017.
 */
@Entity
@Table(name = "payments")
public class Payment {
    @EmbeddedId
    private PaymentPK paymentPK;

    @Column(name = "paymentDate")
    private Date paymentDate;

    @Column(name = "amount")
    private Double amount;

    public Payment() {
    }

    public Payment(Customer customer, String checkNumber) {
        this.paymentPK = new PaymentPK(customer, checkNumber);
    }

    public PaymentPK getPaymentPK() {
        return paymentPK;
    }

    public void setPaymentPK(PaymentPK paymentPK) {
        this.paymentPK = paymentPK;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Payment)) return false;

        Payment payment = (Payment) o;

        return getPaymentPK() != null ? getPaymentPK().equals(payment.getPaymentPK()) : payment.getPaymentPK() == null;
    }

    @Override
    public int hashCode() {
        return getPaymentPK() != null ? getPaymentPK().hashCode() : 0;
    }
}

