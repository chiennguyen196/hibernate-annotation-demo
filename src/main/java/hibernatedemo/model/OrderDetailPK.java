package hibernatedemo.model;

import java.io.Serializable;

/**
 * Created by Chien Nguyen on 23/06/2017.
 */
public class OrderDetailPK implements Serializable {
    protected Order order;
    protected Product product;

    public OrderDetailPK() {
    }

    public OrderDetailPK(Order order, Product product) {
        this.order = order;
        this.product = product;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderDetailPK)) return false;

        OrderDetailPK that = (OrderDetailPK) o;

        if (order != null ? !order.equals(that.order) : that.order != null) return false;
        return product != null ? product.equals(that.product) : that.product == null;
    }

    @Override
    public int hashCode() {
        int result = order != null ? order.hashCode() : 0;
        result = 31 * result + (product != null ? product.hashCode() : 0);
        return result;
    }
}
